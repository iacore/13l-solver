use std::{
    collections::BTreeMap,
    fs::File,
    io::{stdin, BufRead, BufReader},
    sync::Arc,
};

use dashmap::DashMap;
use eyre::{Context, ContextCompat};
use rayon::prelude::{ParallelBridge, ParallelIterator};

#[derive(PartialEq, Eq, Hash, Debug, PartialOrd, Ord)]
struct RadixKey {
    count: u8,
    c: char,
}

// fn counts(this: &[RadixKey]) -> u8 {
//     this.iter().map(|x| x.count).sum()
// }

fn subset_of(this: &[RadixKey], other: &[RadixKey]) -> bool {
    this.iter().all(|this_radix| {
        other.iter().any(|other_radix| {
            this_radix.c == other_radix.c && this_radix.count <= other_radix.count
        })
    })
}

fn word_to_letter_freq(word: &str) -> Box<[RadixKey]> {
    let mut counter: BTreeMap<char, u8> = Default::default();
    for ref c in word.to_ascii_lowercase().chars() {
        if !c.is_ascii_lowercase() {
            continue;
        }
        match counter.get_mut(c) {
            Some(count) => {
                *count += 1;
            }
            None => {
                counter.insert(*c, 1);
            }
        }
    }
    let mut counts: Vec<_> = counter
        .into_iter()
        .map(|(c, count)| RadixKey { c, count })
        .collect();
    counts.sort_by(|b, a| match a.count.cmp(&b.count) {
        std::cmp::Ordering::Less => std::cmp::Ordering::Less,
        std::cmp::Ordering::Greater => std::cmp::Ordering::Greater,
        std::cmp::Ordering::Equal => a.c.cmp(&b.c),
    });
    counts.into()
}

fn find_closest(tree: &DashMap<Box<[RadixKey]>, String>, word: &str) -> Option<String> {
    let freqs = word_to_letter_freq(word);
    // exact match
    if let Some(word) = tree.get(&freqs).map(|x| x.to_owned()) {
        return Some(word);
    }
    // brute force
    tree.into_iter()
        .par_bridge()
        .filter(|x| subset_of(&freqs, x.key()))
        .map(|x| x.value().to_owned())
        .max_by_key(String::len)
}

fn main() -> Result<(), eyre::Error> {
    let re = BufReader::new(File::open("words_alpha.txt")?);
    let tree: Arc<DashMap<_, String>> = Default::default();
    rayon::scope(|scope| {
        for word in re.lines() {
            let word = word?;
            let tree = tree.clone();
            scope.spawn(move |_| {
                tree.insert(word_to_letter_freq(&word), word);
            });
        }
        Ok::<(), eyre::Error>(())
    })?;

    println!("Words loaded");

    if std::env::var_os("browser").is_some() {
        main_browser(tree)
    } else {
        main_terminal(tree)
    }
}

fn main_terminal(tree: Arc<DashMap<Box<[RadixKey]>, String>>) -> Result<(), eyre::Report> {
    let mut line = String::new();
    loop {
        line.clear();
        stdin().read_line(&mut line)?;
        if line.is_empty() {
            break;
        }
        let maybe_word = find_closest(&tree, &line);
        match maybe_word {
            Some(word) => {
                println!("Match: {}", word)
            }
            None => {
                println!("No match")
            }
        }
    }
    Ok(())
}

fn main_browser(tree: Arc<DashMap<Box<[RadixKey]>, String>>) -> Result<(), eyre::Report> {
    use tiny_http::{Response, Server};

    let addr = "localhost:3333";
    let server = Server::http(addr)
        .map_err(|e| eyre::eyre!(e.to_string()))
        .context("setup server")?;

    println!("HTTP Mode started, listening on {}", addr);

    for request in server.incoming_requests() {
        // println!(
        //     "received request! method: {:?}, url: {:?}, headers: {:?}",
        //     request.method(),
        //     request.url(),
        //     request.headers()
        // );

        let word = request
            .url()
            .split("?")
            .skip(1)
            .next()
            .context("should have query string")?;
        let maybe_word = find_closest(&tree, word);
        match maybe_word {
            Some(word) => {
                println!("Match: {}", word);
                request.respond(Response::from_string(word.to_owned()))?;
            }
            None => {
                request.respond(Response::empty(400))?;
            }
        }
    }
    Ok(())
}
